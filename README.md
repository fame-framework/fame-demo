# FAME-Demo
Demo model for FAME - Please visit the [Wiki for FAME](https://gitlab.com/fame-framework/wiki/-/wikis/home) to get an explanation of FAME and its components.
There, you will also find detailed instructions on how to get started with FAME and how to setup FAME-Demo.

## Context
FAME-Demo illustrates how to create a model using FAME-Core library and how to run it using FAME-Core and FAME-Io.
It provides mock Agents that send and receive Messages, perform Actions, use Contracts, Input and Output.
FAME-Demo does not resemble a meaningful energy systems model.
For a full-blow and meaningful model based on FAME, have a look at [AMIRIS](https://gitlab.com/dlr-ve/esy/amiris/amiris), the open Agent-based Market model for the Investigation of Renewable and Integrated energy Systems.

## Agents
FAME-Demo uses five different Agent classes defined in the package `agents`:
* `DemoAgent`: shows how to transmit complex messages using the Portable interface
* `Exchange`: a dummy energy market that receives `Bid` Messages from `Trader` Agents and returns `Award` Messages to them; also demonstrates how to define structured Input to an Agent and how to write complex output
* `Trader`: shows how abstract Agent classes can be used in a class hierarchy; demonstrates inheritance of Actions and Output columns
* `Supply`: demonstrates how to digest messages and how to fulfil Contracts and how to write simple output
* `Demand`: shows random number generation service, how to override inherited Actions and how to send a Message directly

## Messages
Two different types of Messages are used in FAME-Demo: complex Messages in the package `portableItems` that use the Portable interface, as well as less complex Messages in the package `msg` extending FAME's DataItem class.

## Input
FAME-Demo provides a pre-compiled input file at `input/fameDemoRunConfig.pb`.
You can also compile this file yourself utilising FAME-Io and the provided configuration files in the folder `input/yaml/`.
The standard configuration comprises 1 Exchange, 1 Demand, 1 Supply and 2 DemoAgent instances.
Have a look at the file `input/yaml/scenario.yaml` to see how they are parameterised.
Inspect the file `input/yaml/contracts/contracts_demo.yaml` to see how and when these agents perform Actions and send each other Messages.
The simulation runs for one year.
Contracts trigger on a daily basis (i.e. with a `deliveryInterval` of 86400 seconds).

## Output
Running the simulation will create a single output file.
Use FAME-Io to split it into separate files in .csv format.
FAME-Io creates an individual output file for each **type** of agent.
As the `Exchange` Agent features complex output columns, an additional .csv file per complex column is created for `Exchange`.

# Installation instructions
Clone the FAME-Demo repository

	git clone https://gitlab.com/fame-framework/fame-demo.git

Go to the root folder of the newly cloned folder, and execute

	mvn package
	
Wait for Maven to fetch all dependencies and to build FAME-Demo.
This creates a Java archive (JAR) file in the target/ folder that includes FAME-Demo and all of its dependencies.
The file should be named fame-demo-jar-with-dependencies.jar by default.

# Run instructions
FAME applications can be run in parallel mode using multiple processes, or using a single process.
It is strongly recommended to run FAME-Demo in single-process mode (active by default), before unlocking the multi-process mode.
The provided configuration of FAME-Demo should take about one second to complete on a modern desktop computer.
Once FAME-Demo is run, its output files are generated in the "result" folder.

## Multi-Process Mode
Please proceed with the single-process mode first and come back here later.
Once multiple processes shall be used, the [MPJ-Express](http://mpj-express.org/) library is necessary and needs to be installed first.
In addition, the multi-process mode needs to be unlocked via including a dependency.
See also the [dedicated section](https://gitlab.com/fame-framework/wiki/-/wikis/GetStarted/parallel/RunParallel) in the FAME-Wiki on how to run FAME models in multi-process mode.

### Unlock Multi-Process Mode
Open the "pom.xml" file in the FAME-Demo base directory.
In the `<dependencies>` section you will find an additional, commented dependency for `<artifactId>mpi-mpj-impl</artifactId>`.
Activate the dependency by removing the comment lines.

## With Eclipse
### Single Process
You can start FAME-Demo right ahead.
Run the provided launch file "FameDemoNoMPJ.launch" in Eclipse by navigating to Run -> Run Configurations -> Java Application -> FameDemoNoMPJ and clicking the "Run"-button.

### Multiple Processes
First, you need to install MPJ Express Debugger to Eclipse.

#### Install MPJ Express
Download the MPJ-Express Debugger [here](https://sourceforge.net/projects/mpjexpress/files/debugger/). A complete video guide can be found at http://vimeo.com/86049819, and a 20-page HowTo [here](http://mpj-express.org/docs/guides/RunningandDebuggingMPJExpresswithEclipse.pdf). 
- Close Eclipse (if currently running).
- Unzip MPJExpress Debugger file and place it in Eclipse plugin directory.
- (Re)start Eclipse.

#### Run
You can then use the provided file "FameDemo.launch" by navigating to Run -> Run Configurations -> MPJExpress Application -> FameDemo and clicking on the "Run"-button.

## With console
You need an installation of the Java Development Kit (JDK) version 8 or higher.
The `java` command in your console needs to point to that JDK.

### Single Process
In the base directory of FAME-Demo run `java -cp "target/fame-demo-jar-with-dependencies.jar" de.dlr.gitlab.fame.setup.FameRunner -f ./input/fameDemoRunConfig.pb`

### Parallel Processes
First, you need to install MPJ Express to your system, see [MPJ-Express Installation Guides](http://mpj-express.org/guides.html)
#### Run
MPJ-Express offers the possibility to run jobs in parallel on one system without having MPI installed.
If you use this option, FAME-Demo will be executed in parallel on one system.

```
mpjrun.sh -np 2 -ea -cp "target/fame-demo-jar-with-dependencies.jar" de.dlr.gitlab.fame.setup.FameRunner -f ./input/fameDemoRunConfig.pb -m MPJ
```

However, as MPJ emulates this parallelisation, it will not provide actual runtime speed-ups.
To achieve actual speed-up, you need to link to an actual MPI implementation.
See the [dedicated section](https://gitlab.com/fame-framework/wiki/-/wikis/GetStarted/parallel/RunParallel) in the FAME-Wiki on how to link a FAME models to openMPI.