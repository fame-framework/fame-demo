package portableItems;

import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.data.TimeSeries;

/**
 * Example complex object to be transferred using {@link Portable} interface - including a {@link TimeSeries}
 * 
 * @author Christoph Schimeczek
 */
public class ComplexTransferObject implements Portable {
	private int integer;
	private TimeSeries timeSeries;

	public ComplexTransferObject() {}
	
	public ComplexTransferObject(int integer, TimeSeries timeSeries) {
		this.integer = integer;
		this.timeSeries = timeSeries;
	}
	
	@Override
	public void addComponentsTo(ComponentCollector collector) {
		collector.storeTimeSeries(timeSeries);
		collector.storeInts(integer);
	}

	@Override
	public void populate(ComponentProvider provider) {
		timeSeries = provider.nextTimeSeries();
		integer = provider.nextInt();
	}
	
	public int getInteger() {
		return integer;
	}
	
	public TimeSeries getTimeSeries() {
		return timeSeries;
	}
	
	@Override
	public String toString() {
		return "ComplexTransferObject(" + integer + " - " + timeSeries + ")";
	}
}