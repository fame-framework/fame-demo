package portableItems;

import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;


/**
 * Example for a nested item to be transmitted via the {@link Portable} interface
 * 
 * @author Marc Deissenroth
 */
public class InnerComplexTransferObject implements Portable {
	private int integerOne;
	private double doubleOne;
	private String innerName;

	public InnerComplexTransferObject() {}
	
	public InnerComplexTransferObject(int integer, double doub, String name) {
		this.integerOne = integer;
		this.doubleOne = doub;
		this.innerName = name;
	}
	
	@Override
	public void addComponentsTo(ComponentCollector collector) {
		collector.storeInts(integerOne);
		collector.storeDoubles(doubleOne);
		collector.storeStrings(innerName);
	}

	@Override
	public void populate(ComponentProvider provider) {
		integerOne = provider.nextInt();
		doubleOne = provider.nextDouble();
		innerName = provider.nextString();
	}

	/**
	 * @return the integerOne
	 */
	public int getIntegerOne() {
		return integerOne;
	}

	/**
	 * @return the doubleOne
	 */
	public double getDoubleOne() {
		return doubleOne;
	}

	/**
	 * @return the innerName
	 */
	public String getInnerName() {
		return innerName;
	}
	
	@Override
	public String toString() {
		return "(" + innerName + ": " + integerOne + " - " + doubleOne + ")";
	}
}