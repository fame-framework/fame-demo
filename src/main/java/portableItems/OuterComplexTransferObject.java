package portableItems;

import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;

/**
 * Example for an outer item that contains an inner nested item - to be transmitted via the {@link Portable} interface
 * 
 * @author Marc Deissenroth
 */
public class OuterComplexTransferObject implements Portable {
	private int integer;
	private String outerName;
	private InnerComplexTransferObject innerObject;

	public OuterComplexTransferObject() {}
	
	public OuterComplexTransferObject(int integer, String name, InnerComplexTransferObject inner) {
		this.integer = integer;
		this.outerName = name;
		this.innerObject = inner;
}
	
	@Override
	public void addComponentsTo(ComponentCollector collector) {
		collector.storeInts(integer);
		collector.storeStrings(outerName);
		collector.storeComponents(innerObject);
	}

	@Override
	public void populate(ComponentProvider provider) {
		integer = provider.nextInt();
		outerName = provider.nextString();
		innerObject = provider.nextComponent(InnerComplexTransferObject.class);
	}
	
	public int getInteger() {
		return integer;
	}
	
	@Override
	public String toString() {
		return "(" + outerName + ": " + integer + " - " + innerObject + ")";
	}	
}