package msg;

import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem.Builder;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.time.TimePeriod;
import de.dlr.gitlab.fame.time.TimeSpan;
import de.dlr.gitlab.fame.time.TimeStamp;

public class Bid extends DataItem {
	public final TimePeriod biddingPeriod;
	public final double energyInMWH;
	public final double priceInEURperMWH;

	/**
	 * Constructs a new {@link Bid}
	 * 
	 * @param validAt
	 *          time period to be associated with this bid
	 * @param energyInMWH
	 *          offered or requested energy
	 * @param priceInEURperMWH
	 *          price of bid in Euro per MWH
	 */
	public Bid(TimePeriod validAt, double energyInMWH, double priceInEURperMWH) {
		this.biddingPeriod = validAt;
		this.energyInMWH = energyInMWH;
		this.priceInEURperMWH = priceInEURperMWH;
	}

	/**
	 * Constructs a new {@link Bid} from its protobuf representation
	 * 
	 * @param proto
	 *          a ProtoDataItem for the Bid
	 */
	public Bid(ProtoDataItem proto) {
		this.biddingPeriod = new TimePeriod(new TimeStamp(proto.getLongValue(0)), new TimeSpan(proto.getLongValue(1)));
		this.energyInMWH = proto.getDoubleValue(0);
		this.priceInEURperMWH = proto.getDoubleValue(1);
	}

	/** Serialises this Bid with the help of DataItem protobuf builder */
	@Override
	protected void fillDataFields(Builder builder) {
		builder.addLongValue(biddingPeriod.getStartTime().getStep());
		builder.addLongValue(biddingPeriod.getDuration().getSteps());
		builder.addDoubleValue(energyInMWH);
		builder.addDoubleValue(priceInEURperMWH);
	}
}