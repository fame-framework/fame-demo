package msg;

import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem.Builder;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.time.TimePeriod;
import de.dlr.gitlab.fame.time.TimeSpan;
import de.dlr.gitlab.fame.time.TimeStamp;

public class PowerAtTime extends DataItem {
	public final TimePeriod validAt;
	public final double powerInMW;

	/**
	 * Constructs a new {@link PowerAtTime}
	 * 
	 * @param validAt
	 *          TimePeriod for which this power is assigned
	 * @param powerInMW
	 *          average power to be delivered during the given time Segment
	 */
	public PowerAtTime(TimePeriod validAt, double powerInMW) {
		this.validAt = validAt;
		this.powerInMW = powerInMW;
	}

	/**
	 * Constructs a new {@link PowerAtTime} from its protobuf representation
	 * 
	 * @param proto
	 *          ProtoDataItem of power at time
	 */
	public PowerAtTime(ProtoDataItem proto) {
		validAt = new TimePeriod(new TimeStamp(proto.getLongValue(0)), new TimeSpan(proto.getLongValue(1)));
		powerInMW = proto.getDoubleValue(0);
	}

	@Override
	/** Serialises this PowerAtTime with the help of DataItem protobuf builder */
	protected void fillDataFields(Builder builder) {
		builder.addLongValue(validAt.getStartTime().getStep());
		builder.addLongValue(validAt.getDuration().getSteps());
		builder.addDoubleValue(powerInMW);
	}
}