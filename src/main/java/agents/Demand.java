package agents;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.agent.input.Input;
import de.dlr.gitlab.fame.agent.input.Make;
import de.dlr.gitlab.fame.agent.input.ParameterData;
import de.dlr.gitlab.fame.agent.input.Tree;
import de.dlr.gitlab.fame.agent.input.ParameterData.MissingDataException;
import de.dlr.gitlab.fame.communication.CommUtils;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.Product;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.time.Constants.Interval;
import de.dlr.gitlab.fame.time.TimeOfDay;
import de.dlr.gitlab.fame.time.TimePeriod;
import de.dlr.gitlab.fame.time.TimeSpan;
import de.dlr.gitlab.fame.time.TimeStamp;
import msg.Bid;
import msg.PowerAtTime;

/** Dummy implementation of a Demand Trader. Whenever the agent is contracted to provide {@link Products#DemandBids} it prepares 1
 * bid for each hour of the following day (24 in total). Whenever the agent is contracted to receive
 * {@link agents.Exchange.Products#AwardedBids} from the {@link Exchange} it checks these Awards and writes the total sum of
 * awarded power to file in the column named {@link OutputFields#AwardedPower24h}. Defined action in Trader is overwritten
 * 
 * @author Christoph Schimeczek */
// Every Agent needs to inherited from FAME-Core's "Agent" class - or of course any of its children
public class Demand extends Trader {
	/** The name of all the products this agent can provide in Contracts */
	@Product
	public static enum Products {
		DemandBids
	};

	// No need to define OutputFields - columns are inherited from trader

	/** The name of the agent's input fields */
	@Input static final Tree parameters = Make.newTree().add(
			Make.newSeries("DemandSeries").help("This specifies the hourly energy demand in MWh/h")).buildTree();

	/** A TimeSeries of the energy this Agent is requiring over the course of time */
	private final TimeSeries tsDemand;
	private final Random randomNumberGenerator;

	/** Constructs a {@link Demand} Agent which requests energy from the energy exchange
	 * 
	 * @param dataProvider input data object handed over from FAME-Core - do not change this
	 * @throws MissingDataException thrown if any input data are missing */
	public Demand(DataProvider dataProvider) throws MissingDataException {
		super(dataProvider); // Java enforces to call the constructors of parent classes first
		// Extract all the raw input data in protobuf format in the dataProvider to match your ParameterTree definitions
		ParameterData inputData = parameters.join(dataProvider);
		// Retrieve input - if the input cannot be found a MissingDataException is thrown
		tsDemand = inputData.getTimeSeries("DemandSeries");
		// Use FAME-Core's built-in distributed random number generator - check out its JavaDoc
		randomNumberGenerator = getNextRandomNumberGenerator();

		// Define actions to be executed for configured contracts
		// Action: Send bids for required energy to energy exchange once per day
		call(this::prepareBids).on(Products.DemandBids);
		// Action: call(this::digestAwards) is define in Trader but automatically overridden with the override of the function
	}

	/** Send 24 Bids to the energy exchange
	 * 
	 * @param incomingMessages any incoming {@link Message}s associated with this action - in this case none -> an empty list
	 * @param contracts all {@link Contract}s that schedule this action - in our case one Contract with Exchange */
	private void prepareBids(ArrayList<Message> incomingMessages, List<Contract> contracts) {
		// There should be only one exchange to send bids to, so get the corresponding Contract
		Contract contractWithExchange = CommUtils.getExactlyOneEntry(contracts);
		// In order to address messages to the exchange, its ID is needed - it can be obtained from the Contract
		long exchangeId = contractWithExchange.getReceiverId();
		// To let the Exchange know which Contract we are referring to with our message: Create a DataItem specific to this contract
		// also indicating for which TimeStamp the Contract will be fulfilled
		ContractData contractData = contractWithExchange.fulfilAfter(now());

		// The Bids we are about to send start at 00:00h of the next day- thus we need to find the corresponding TimeStamp
		TimeStamp lastHourOfToday = now().nextTimeOfDay(TimeOfDay.END_OF_DAY);
		for (int hourIndex = 0; hourIndex < 24; hourIndex++) {
			// Create a TimeStamp matching the first second of each hour of the next day
			TimeStamp startOfTimePeriod = lastHourOfToday.nextTimeOfDay(new TimeOfDay(hourIndex, 0, 0));
			// Create a TimePeriod that lasts for exactly one full hour of the next day
			TimePeriod timePeriod = new TimePeriod(startOfTimePeriod, new TimeSpan(1, Interval.HOURS));
			// Randomly create a price we are willing to pay for the requested energy
			double randomEnergyPrice = randomNumberGenerator.nextDouble() * 100.0;
			// Read the required energy demand from the TimeSeries provided by the input file
			double requestedEnergy = tsDemand.getValueLinear(startOfTimePeriod);
			// Create the Bid from the calculated data; it is Portable, thus can be attached to a message
			Bid bid = new Bid(timePeriod, requestedEnergy, randomEnergyPrice);
			// Send an individual message per Bid since each message can only contain one Portable
			sendMessageTo(exchangeId, bid, contractData);
			// There is also a shortcut available - see implementation at Supply Agent
		}
	}

	/** Evaluate received awarded power and write their sum to output - see for a more compact form in {@link Trader} <br />
	 * It is not required to define the action anew - it is automatically bound to this implementation */
	@Override
	protected void digestAwards(ArrayList<Message> incomingMessages, List<Contract> contracts) {
		double awardedPower = 0;
		for (Message message : incomingMessages) {
			// Check if message has the required DataItem, otherwise ignore this message.
			if (!message.containsType(PowerAtTime.class))
				continue;
			// Retrieve the required DataItem from the message.
			PowerAtTime powerItem = message.getDataItemOfType(PowerAtTime.class);
			// Get the value of awarded power from the message item and add it to the sum.
			awardedPower += powerItem.powerInMW;
		}
		// Write the sum of awardedPower to the corresponding output field - field is defined in Trader
		store(OutputFields.AwardedPower24h, awardedPower);
	}
}