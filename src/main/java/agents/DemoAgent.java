package agents;

import java.util.ArrayList;
import java.util.List;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.agent.input.Input;
import de.dlr.gitlab.fame.agent.input.Make;
import de.dlr.gitlab.fame.agent.input.ParameterData;
import de.dlr.gitlab.fame.agent.input.Tree;
import de.dlr.gitlab.fame.agent.input.ParameterData.MissingDataException;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.Product;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.service.output.Output;
import portableItems.ComplexTransferObject;
import portableItems.InnerComplexTransferObject;
import portableItems.OuterComplexTransferObject;

/** Demonstration agent to show transmission of complex messages with full serialisation including nested inner objects
 * 
 * @author Marc Deissenroth */
public class DemoAgent extends Agent {
	@Product
	public static enum Products {
		OneTimeHyperComplexMessage, OneTimeSuperComplexMessage, OneTimeComplexMessage
	}

	@Output
	private static enum OutputFields {
		OuterComplexDataId
	};

	@Input static final Tree parameters = Make.newTree().add(Make.newSeries("DummyTimeSeries")).buildTree();

	private final TimeSeries dummyTimeSeries;

	public DemoAgent(DataProvider dataProvider) throws MissingDataException {
		super(dataProvider);
		ParameterData inputData = parameters.join(dataProvider);
		dummyTimeSeries = inputData.getTimeSeries("DummyTimeSeries");

		call(this::sendOrDigestNestedObject).on(Products.OneTimeHyperComplexMessage)
				.use(Products.OneTimeHyperComplexMessage);
		call(this::sendOrDigestNestedObject).on(Products.OneTimeSuperComplexMessage)
				.use(Products.OneTimeSuperComplexMessage);
		call(this::sendOrDigestComplexMessage).on(Products.OneTimeComplexMessage).use(Products.OneTimeComplexMessage);

	}

	/** sends or digests a nested object message according to the given {@link Contract} */
	private void sendOrDigestNestedObject(ArrayList<Message> incomingMessages, List<Contract> contracts) {
		Contract demoContract = contracts.get(0);
		if (isSenderOf(demoContract)) {
			sendNestedObjectMessage(demoContract);
		} else {
			receiveOneNestedObject(incomingMessages.get(0));
		}
	}

	private void receiveOneNestedObject(Message message) {
		OuterComplexTransferObject port = message.getAllPortableItemsOfType(OuterComplexTransferObject.class).get(0);
		System.out.println(this + " received complex message from sender " + message.getSenderId() + ":: " + port);
		store(OutputFields.OuterComplexDataId, port.getInteger());
	}

	/** @return true if the Sender of the given {@link Contract} is this {@link Agent} */
	private boolean isSenderOf(Contract contract) {
		return getId() == contract.getSenderId();
	}

	/** sends message with a nested object to the given {@link Contract}'s receiver */
	private void sendNestedObjectMessage(Contract demoContract) {
		long receiverId = demoContract.getReceiverId();
		OuterComplexTransferObject octo = new OuterComplexTransferObject(1701, "outerData",
				new InnerComplexTransferObject(42, 432.1, "innerData"));
		ContractData contractData = new ContractData(demoContract.getContractId(),
				demoContract.getNextTimeOfDeliveryAfter(now()));
		sendMessageTo(receiverId, octo, contractData);
	}

	/** sends or receives a {@link Portable} object according to the given {@link Contract} */
	private void sendOrDigestComplexMessage(ArrayList<Message> incomingMessages, List<Contract> contracts) {
		Contract contract = contracts.get(0);
		if (isSenderOf(contract)) {
			long receiverId = contract.getReceiverId();
			ComplexTransferObject cto = new ComplexTransferObject(42, dummyTimeSeries);
			ContractData contractData = new ContractData(contract.getContractId(),
					contract.getNextTimeOfDeliveryAfter(now()));
			sendMessageTo(receiverId, cto, contractData);
		} else {
			Message message = incomingMessages.get(0);
			ComplexTransferObject cto = message.getAllPortableItemsOfType(ComplexTransferObject.class).get(0);
			System.out.println(this + " received from agent " + message.getSenderId() + ":: " + cto);
		}
	}
}