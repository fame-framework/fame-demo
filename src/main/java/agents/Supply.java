package agents;

import java.util.ArrayList;
import java.util.List;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.agent.input.Input;
import de.dlr.gitlab.fame.agent.input.Make;
import de.dlr.gitlab.fame.agent.input.ParameterData;
import de.dlr.gitlab.fame.agent.input.Tree;
import de.dlr.gitlab.fame.agent.input.ParameterData.MissingDataException;
import de.dlr.gitlab.fame.communication.CommUtils;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.Product;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.service.output.Output;
import de.dlr.gitlab.fame.time.Constants.Interval;
import de.dlr.gitlab.fame.time.TimeOfDay;
import de.dlr.gitlab.fame.time.TimePeriod;
import de.dlr.gitlab.fame.time.TimeSpan;
import de.dlr.gitlab.fame.time.TimeStamp;
import msg.Bid;

/** Dummy implementation of a Supply Trader. Whenever the agent is contracted to provide {@link Products#SupplyBids} it prepares 1
 * bid for each hour of the following day (24 in total).<br />
 * Inherited Action from {@link Trader}: Whenever the agent is contracted to receive {@link agents.Exchange.Products#AwardedBids}
 * from the {@link Exchange} it checks these Awards and writes the total sum of awarded power to file in the column named
 * {@link OutputFields#AwardedPower24h}.
 * 
 * @author Christoph Schimeczek */
public class Supply extends Trader {
	/** The name of all the products this Agent can provide in Contracts */
	@Product
	public static enum Products {
		SupplyBids
	};

	/** Output columns are automatically inherited from Trader.OutputFields; here an additional column for this Agent is defined */
	@Output
	private static enum OutputFields {
		ActualOutput
	};

	/** The structure of the Agent's input fields */
	@Input static final Tree parameters = Make.newTree().add(Make.newSeries("SupplySeries")).buildTree();

	/** A TimeSeries representing the energy this Agent could potentially supply over the course of time */
	private final TimeSeries tsSupply;

	/** Constructs a {@link Supply} Agent which offers energy to the energy exchange
	 * 
	 * @param dataProvider input data object handed over from FAME-Core - do not change this
	 * @throws MissingDataException thrown if any input data are missing */
	public Supply(DataProvider dataProvider) throws MissingDataException {
		// Java enforces to call the constructors of parent classes first
		super(dataProvider);
		// Extract all the raw input data in protobuf format in the dataProvider to match your ParameterTree definitions
		ParameterData inputData = parameters.join(dataProvider);
		// Retrieve input - if the input cannot be found a MissingDataException is thrown
		tsSupply = inputData.getTimeSeries("SupplySeries");

		// Define actions to be executed for configured contracts
		// 1st Action: Send bids for potential energy supply to energy exchange once per day
		call(this::prepareBids).on(Products.SupplyBids);
		// 2nd Action: override Trader's Action for AwardedBids and do something completely different on that occasion
		call(this::doSomthingElse).on(Exchange.Products.AwardedBids).use(Exchange.Products.AwardedBids);
	}

	/** Send 24 Bids to the energy exchange
	 * 
	 * @param incomingMessages any incoming {@link Message}s associated with this action - in this case none -> an empty list
	 * @param contracts all {@link Contract}s that schedule this action - in our case one Contract with Exchange */
	private void prepareBids(ArrayList<Message> incomingMessages, List<Contract> contracts) {
		// There should be only one exchange to send bids to, so get the corresponding Contract
		Contract contractWithExchange = CommUtils.getExactlyOneEntry(contracts);
		// The Bids we are about to send start at 00:00h of the next day- thus we need to find the corresponding TimeStamp
		TimeStamp lastHourOfToday = now().nextTimeOfDay(TimeOfDay.END_OF_DAY);
		for (int hourIndex = 0; hourIndex < 24; hourIndex++) {
			// Create a TimeStamp matching the first second of each hour of the next day
			TimeStamp startOfTimePeriod = lastHourOfToday.nextTimeOfDay(new TimeOfDay(hourIndex, 0, 0));
			// Create a TimePeriod that lasts for exactly one full hour of the next day
			TimePeriod timePeriod = new TimePeriod(startOfTimePeriod, new TimeSpan(1, Interval.HOURS));
			// Read the potential energy supply from the TimeSeries provided by the input file
			double potentialEnergySupply = tsSupply.getValueLinear(startOfTimePeriod);
			// Create the Bid from the calculated data; it is Portable, thus can be attached to a message
			Bid bid = new Bid(timePeriod, potentialEnergySupply, 42.);
			// Fulfill the given Contract for the next occasion after now() - sends a Message to Exchange containing the given Bid
			fulfilNext(contractWithExchange, bid);
			// There is a more expressive implementation to fulfill Contracts available in the Demand-Agent
		}
	}

	/** Instead of writing the awarded power, Supply writes the supreme number 42 to its AdditionalOutput column
	 * 
	 * @param incomingMessages incoming {@link Message}s associated with this action; here: Awards from Exchange
	 * @param contracts all {@link Contract}s that schedule this action; here: one Contract with Exchange */
	protected void doSomthingElse(ArrayList<Message> incomingMessages, List<Contract> contracts) {
		store(OutputFields.ActualOutput, 42.);
	}
}
