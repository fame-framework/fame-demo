package agents;

import java.util.ArrayList;
import java.util.List;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.agent.input.Input;
import de.dlr.gitlab.fame.agent.input.Make;
import de.dlr.gitlab.fame.agent.input.ParameterData;
import de.dlr.gitlab.fame.agent.input.ParameterData.MissingDataException;
import de.dlr.gitlab.fame.agent.input.Tree;
import de.dlr.gitlab.fame.communication.CommUtils;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.Product;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;
import de.dlr.gitlab.fame.time.TimeStamp;
import msg.Bid;
import msg.PowerAtTime;

/** Dummy implementation of an energy exchange Agent. Exchange clears the market (stubbed) based on previously received Bids from
 * Demand and Supply. It has can handle connections with multiple Demand and Supply Agents. Exchange shows also how to construct a
 * complex input data ParameterTree and how to extract data from the input file.
 *
 * @author Christoph Schimeczek */
public class Exchange extends Agent {
	/** Names of all products this Agent can provide in Contracts */
	@Product
	public static enum Products {
		AwardedBids
	}

	/** Names of columns in this Agent's file output */
	@Output
	private static enum OutputFields {
		ReceivedBidCount, BidEnergy
	};

	/** Names inner column labels for a complex output column */
	private enum BidLabels {
		From, Time
	};

	/** Defines a ComplexIndex "bidIndex" for the output column "Bids" using the "BidLabels" defined above. This transforms the
	 * output column "Bids" to a table and allows saving more than one value per time step distinguished by the inner index key
	 * columns "From" and "Time" specified in "BidLabels". Note that the addressed output column enum must be annotated with
	 * {@link Output} and that the {@link ComplexIndex} field must be "static" and "final". */
	private static final ComplexIndex<BidLabels> bidIndex = ComplexIndex.build(OutputFields.BidEnergy, BidLabels.class);

	/** A sample Enum whose value is read from file */
	private static enum MyDummyEnum {
		CHOICE_A, CHOICE_B
	}

	/** Definition of a parameter group - this could also be part of any component of Exchange; Note that all elements are on the
	 * same level in this group. Parameter Tree creation employs builders taken from the {@link Make} interface. Note, this
	 * ParameterTree is not immediately used to define the file input, since it lacks the @Input annotation. Note further, that
	 * stand-alone parameter groups can only be create with newTree() - newGroup() should only be used inside an add() statement */
	private static final Tree parameterGroup = Make.newTree()
			.add(Make.newInt("Int1").fill("42"), // add integer with name "Int1" and fill with "42" if value is missing
					Make.newInt("Int2").help("This text is shown during simulation setup!"), // add integer with name "Int2" with comment
					Make.newDouble("Double1") // and a double value parameter named "Double1"
			).buildTree(); // close "add" and finalise definition to create an unnamed Parameter Group

	@Input // This marks the following element as the base ParameterTree defining the file input structure
	private static final Tree parameters = Make.newTree().add( // request a new ParameterTreeBuilder and starts adding
			Make.newGroup("Group1").add( // new group "Group1" is defined - adding to it is started
					Make.newDouble("Double1"), // add inner element "Double1" to "Group1"
					Make.newGroup("InnerGroup").add( // "InnerGroup" is created within "Group1" - adding to it is started
							Make.newEnum("Choose", MyDummyEnum.class), // parameter of type MyDummyEnum is added to "InnerGroup" named "Choose"
							Make.newDouble("List1").list() // a list of doubles is added to "InnerGroup" named "List1"
					).list() // define "InnerGroup" as List -> all of its elements can be repeated
			)) // leave "InnerGroup" and "Group1" -> we are now back at the top level
			.addAs("Group2", parameterGroup) // add all elements of previously defined "parameterGroup" under name "Group2" to top level
			.add(Make.newDouble("OptionalParam").optional()) // add "OptionalParam" - a double that needs not be defined at top level
			.buildTree();// finalise and retrieve ParameterTree from previous definition

	/** Constructs an Exchange Agent
	 * 
	 * @param dataProvider input data object handed over from FAME-Core - do not change this
	 * @throws MissingDataException thrown if any input data are missing */
	public Exchange(DataProvider dataProvider) throws MissingDataException {
		super(dataProvider); // Java enforces to call the constructors of parent classes first
		// Extract all the raw input data from the dataProvider to match your ParameterTree definitions
		ParameterData inputData = parameters.join(dataProvider);
		readParameters(inputData);

		// Define actions to be executed for configured contracts:
		// Whenever Exchange promised to send AwardedBids, use previously incoming
		// SupplyBids and DemandBids to clear the market
		call(this::clearMarket).on(Products.AwardedBids).use(Supply.Products.SupplyBids, Demand.Products.DemandBids);
		// Inputs can be re-used as triggers for or input to other actions. However, try to join actions when possible since each
		// action definition comes with some overhead and re-used message are copied. This action logs outputs.
		call(this::outputs).on(Supply.Products.SupplyBids).use(Supply.Products.SupplyBids, Demand.Products.DemandBids);
	}

	/** Reads parameters from the {@link ParameterData} instance
	 * 
	 * @param inputData provides data in the structure of a previously defined input parameter {@link Tree}
	 * @throws MissingDataException if any input data are missing */
	private void readParameters(ParameterData inputData) throws MissingDataException {
		// One can directly retrieve input parameters by using the corresponding typed function get<Type>("PathToParameter")
		// "PathToParameter" uses notation "x.y" to denote that "y" is inner element of "x".
		// Several Parameters are printed to console for demonstration:
		readAndPrintInput("Group1.Double1", inputData.getDouble("Group1.Double1"));
		readAndPrintInput("Group2.Double1", inputData.getDouble("Group2.Double1"));
		readAndPrintInput("Group2.Int1", inputData.getInteger("Group2.Int1"));
		readAndPrintInput("Group2.Int2", inputData.getInteger("Group2.Int2"));

		// One can also extract a group of parameters - for the returned ParameterData the group's path is automatically added.
		// This can be used, e.g., to hand over a subset of the input data to an Agent's component.
		ParameterData group2 = inputData.getGroup("Group2");
		readAndPrintInput("Group2.Int2", group2.getInteger("Int2")); // Same output as above, but path is simpler

		// The "InnerGroup" was defined as List. Thus it can contain multiple elements with the same structure.
		// All of the items can be accessed by the Groups name - using the getGroupList() method:
		List<ParameterData> innerGroupEntries = inputData.getGroupList("Group1.InnerGroup");
		readAndPrintInput("Group1.InnerGroup.0.Choose", innerGroupEntries.get(0).getEnum("Choose", MyDummyEnum.class));
		readAndPrintInput("Group1.InnerGroup.1.Choose", innerGroupEntries.get(1).getEnum("Choose", MyDummyEnum.class));

		// Parameters can also be a list: The getList() method returns all of the values attached to a list Parameter:
		readAndPrintInput("Group1.InnerGroup.0.List1", innerGroupEntries.get(0).getList("List1", Double.class));

		// Sometimes a parameter is not present in the input file because it was declared optional when accessing the optional
		// parameter you can provide a default; note the info message in the console
		inputData.getDoubleOrDefault("OptionalParam", 333.0);

		// Alternatively, you can deal with the resulting MissingDataException
		try {
			inputData.getDouble("OptionalParam");
		} catch (MissingDataException e) {
			System.out.println("Exchange catched exception: " + e.getMessage());
		}
	}

	/** Print the given Parameter address and value to console */
	private void readAndPrintInput(String address, Object value) {
		System.out.println("Exchange Parameter: " + address + " -> " + value);
	}

	/** Dummy implementation: market is not cleared but for each contracted partner their bids are returned with awarded half the
	 * offered / requested energy */
	private void clearMarket(ArrayList<Message> incomingMessages, List<Contract> contracts) {
		// Extract the messages containing a Bid - Message which do not contain a Bid remain in incomingMessages
		ArrayList<Message> withBid = CommUtils.extractMessagesWith(incomingMessages, Bid.class);
		// Fulfil each contract on "ToDo-list"
		for (Contract contract : contracts) {
			fulfilContract(withBid, contract);
		}
	}

	/** Sends "PowerAtTime" messages to the contracted partner for each TimePeriod where a Bid was provided */
	private void fulfilContract(ArrayList<Message> messagesWithBid, Contract contract) {
		// Extract all messages from the given Message list that came from current contract partner
		ArrayList<Message> relatedMessages = CommUtils.extractMessagesFrom(messagesWithBid, contract.getReceiverId());
		// For each received message: Respond by returning half the offered / requested energy for the respective TimePeriod
		for (Message message : relatedMessages) {
			// extract the Bid from the received message
			Bid bid = message.getDataItemOfType(Bid.class);
			// Create response: PowerAtTime item for the corresponding bidding period with half the offered / requested energy
			PowerAtTime powerAtTime = new PowerAtTime(bid.biddingPeriod, bid.energyInMWH / 2);
			// fulfil the given contract by sending the created response object to the contract partner
			fulfilNext(contract, powerAtTime);
		}
	}

	/** Writes the total count of received messages to column in the output file */
	private void outputs(ArrayList<Message> incomingMessages, List<Contract> contracts) {
		// Use the store() command to either write a single value per agent and tick to a simple output column...
		store(OutputFields.ReceivedBidCount, incomingMessages.size());

		// ... or write multiple values to an inner table of a column using the defined ComplexIndex
		for (Message message : incomingMessages) {
			Bid bid = message.getDataItemOfType(Bid.class);
			TimeStamp startTime = bid.biddingPeriod.getStartTime();
			long requestingAgent = message.getSenderId();
			// Call "key()" once for >every< index label and set the value for each key of inner table.
			// Calls to key() can be concatenated for convenience.
			// store() automatically clears the given ComplexIndex - so it can be reused without extra effort.
			// Here, we store the "energy" for each bid distinguished by "From" whom and for what "Time" is was sent to the output
			// column "BidEnergy"
			store(bidIndex.key(BidLabels.From, requestingAgent).key(BidLabels.Time, startTime), bid.energyInMWH);
		}
	}
}