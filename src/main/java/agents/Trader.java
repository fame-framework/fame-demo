package agents;

import java.util.ArrayList;
import java.util.List;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.CommUtils;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.service.output.Output;
import msg.PowerAtTime;

/** A sample abstraction of an Agent that trades with the energy exchange.<br />
 * All @Output fields are automatically added to children of Trader.<br />
 * All defined Actions are automatically loaded for all children of Trader as well.<br />
 * Instantiable children are {@link Supply} and {@link Demand}
 *
 * @author Christoph Schimeczek */
public abstract class Trader extends Agent {

	/** Column names for file output for all added to all children of {@link Trader}<br />
	 * Set to "protected" to make them addressable within child classes. */
	@Output
	protected static enum OutputFields {
		AwardedPower24h
	};

	/** Constructs {@link Trader} and sets common actions for children of this class
	 * 
	 * @param dataProvider input data object handed over from FAME-Core - do not change this */
	public Trader(DataProvider dataProvider) {
		// Java enforces to call the constructors of parent classes first
		super(dataProvider);
		// Define actions to be executed for configured contracts:
		// Receive award messages for energy from energy exchange once per day and digest them
		call(this::digestAwards).on(Exchange.Products.AwardedBids).use(Exchange.Products.AwardedBids);
	}

	/** Evaluate received awarded power and write their sum to output - see for a lengthy version in {@link Demand} */
	protected void digestAwards(ArrayList<Message> incomingMessages, List<Contract> target) {
		double awardedPower = CommUtils.extractMessagesWith(incomingMessages, PowerAtTime.class).stream()
				.mapToDouble(m -> m.getDataItemOfType(PowerAtTime.class).powerInMW).sum();
		store(OutputFields.AwardedPower24h, awardedPower);
	}
}
