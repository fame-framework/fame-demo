# [Bagian - v1.1 (2022-07-08)](https://gitlab.com/fame-framework/fame-demo/-/releases/v1.1)
## major changes
* Added: Demo of ComplexIndex mechanics to store more than one value per column for one agent and time step

## minor changes
* improved formatting for JavaDoc and explanatory comments

## / Compatibility
* FAME-Core: >= 1.4
* FAME-Io >= 1.6